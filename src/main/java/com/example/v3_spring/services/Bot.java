package com.example.v3_spring.services;

import org.springframework.stereotype.Component;

@Component
public class Bot implements Robot {
  @Override
  public String getName() {
    return "Bot injected by Spring";
  }
}
