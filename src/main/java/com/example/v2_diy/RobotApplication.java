package com.example.v2_diy;

import com.example.v2_diy.services.Bot;
import com.example.v2_diy.services.Client;

public class RobotApplication {
  public static void main(String[] args) {
    System.out.println(new Client(new Bot()).greet());
  }
}
