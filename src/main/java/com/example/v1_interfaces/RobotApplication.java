package com.example.v1_interfaces;

import com.example.v1_interfaces.services.Client;

public class RobotApplication {
  public static void main(String[] args) {
    System.out.println(new Client().greet());
  }
}
