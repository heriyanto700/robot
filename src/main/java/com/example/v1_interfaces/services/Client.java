package com.example.v1_interfaces.services;

public class Client {
  private final Robot robot;

  public Client() {
    robot = new Bot();
  }

  public String greet() {
    return "Hello robot " + robot.getName();
  }
}
